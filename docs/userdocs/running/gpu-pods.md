:exclamation: In this section you will request GPUs. Make sure you don't waste those and delete your pods when not using the GPUs.

#### Running GPU pods

Use this definition to create your own pod and deploy it to kubernetes:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: gpu-pod-example
spec:
  containers:
  - name: gpu-container
    image: gitlab-registry.nrp-nautilus.io/prp/jupyter-stack/prp:latest
    command: ["sleep", "infinity"]
    resources:
      limits:
        nvidia.com/gpu: 1
```

This example requests 1 GPU device. You can have up to 8 per node if you're [using jobs](/userdocs/running/jobs/), and up to 2 for pods. If you request GPU devices in your pod, 
kubernetes will auto schedule your pod to the appropriate node. There's no need to specify the location manually.

**You should always delete your pod** when your computation is done to let other users use the GPUs.
Consider using [Jobs](/userdocs/running/jobs/) **with actual script instead of `sleep`** whenever possible to ensure your pod is not wasting GPU time.
If you have never used Kubernetes before, see the [tutorial](/userdocs/tutorial/intro).

#### Requesting high-demand GPUs


Certain kinds of GPUs have much higher specs than the others, and to avoid wasting those for regular jobs, your pods will only be scheduled on those if you request the type explicitly.

Currently those include:

* NVIDIA-TITAN-RTX
* NVIDIA-RTX-A5000
* Quadro-RTX-6000
* Tesla-V100-SXM2-32GB
* NVIDIA-A40
* NVIDIA-RTX-A6000
* Quadro-RTX-8000

A100 is running in [MIG mode](#mig-mode) and is not considered high-demand one.

#### Requesting many GPUs


Since 1 and 2 GPU jobs are blocking nodes from getting 4 and 8-GPU jobs, there are some nodes reserved for those. Once you submit a job with 4 or 8 GPUs request, a controller will automatically add toleration which will allow you to use the node reserved for more GPUs. You don't need to do anything manually for that.

#### Choosing GPU type 

We have a variety of GPU flavors attached to Nautilus. This table describes the types of GPUs available for use, but is not up to date - it's better to use the actual cluster information (f.e. `kubectl get nodes -L nvidia.com/gpu.product`).

If you need more graphical memory, use this table or official specs to choose the type:

GPU Type | Memory size (GB)
---|---
NVIDIA-GeForce-GTX-1070 | 8G
NVIDIA-GeForce-GTX-1080 | 8G
Quadro-M4000 | 8G
NVIDIA-A100-PCIE-40GB-MIG-2g.10gb | 10G
NVIDIA-GeForce-GTX-1080-Ti | 12G
NVIDIA-GeForce-RTX-2080-Ti | 12G
NVIDIA-TITAN-Xp | 12G
Tesla-T4 | 16G
NVIDIA-A10 | 24G
NVIDIA-GeForce-RTX-3090 | 24G
NVIDIA-GeForce-RTX-3090 | 24G
NVIDIA-TITAN-RTX | 24G
NVIDIA-RTX-A5000 | 24G
Quadro-RTX-6000 | 24G
Tesla-V100-SXM2-32GB | 32G
NVIDIA-A40 | 48G
NVIDIA-RTX-A6000 | 48G
Quadro-RTX-8000 | 48G

**NOTE**: [Not all nodes are available to all users](/userdocs/running/special/). You can consult about your available resources in [Matrix](/userdocs/start/contact) and on [resources page](https://portal.nrp-nautilus.io/resources). 
Labs connecting their hardware to our cluster have preferential access to all our resources.

To use a **specific type of GPU**, add the affinity definition to you pod yaml
file. The example below specifies *1080Ti* GPU:
```yaml
spec:
 affinity:
   nodeAffinity:
     requiredDuringSchedulingIgnoredDuringExecution:
       nodeSelectorTerms:
       - matchExpressions:
         - key: nvidia.com/gpu.product
           operator: In
           values:
           - NVIDIA-GeForce-GTX-1080-Ti
```

**To make sure you did everything correctly** after you've submited the job, look at the corresponding pod yaml (`kubectl get pod ... -o yaml`) and check that resulting nodeAffinity is as expected.

#### MIG mode

A100 GPUs allow slicing those into several logical GPUs ( [MIG mode](https://docs.nvidia.com/datacenter/tesla/mig-user-guide/index.html#a100-profiles) ). This mode is enabled in our cluster. Things can change, but currently we're thinking about slicing those in halves. The current MIG mode can be obtained from nodes via the `nvidia.com/gpu.product` label: `NVIDIA-A100-PCIE-40GB-MIG-2g.10gb` means 2 compute instances (out of 7 total) and 10GB memory per virtual GPU.
