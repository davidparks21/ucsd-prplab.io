We have a [globus-connect][1] Gitlab repository that provides step by step
instructions and all needed files for running globus-connect container and
creating a personal globus endpoint.

[1]: https://gitlab.nrp-nautilus.io/prp/globus-connect
