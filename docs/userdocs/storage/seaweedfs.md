[SeaweedFS](https://github.com/chrislusf/seaweedfs) is a high-performance distributed filesystem, optimized for working with huge number of files and also huge files.

**The current status is experimental, use at your own risk. Don't put there any data you don't want to lose, or data you can't easily share with others.**

### Currently available storageClasses:

<table>
  <thead>
    <tr class="header">
      <th>StorageClass</th>
      <th>Filesystem Type</th>
      <th>Region</th>
      <th>AccessModes</th>
      <th>Storage Type</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td markdown="span">seaweedfs-storage-hdd</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span">Spinning drives</td>
      <td markdown="span">24 TB</td>
    </tr>
    <tr>
      <td markdown="span">seaweedfs-storage-nvme</td>
      <td markdown="span">SeaweedFS</td>
      <td markdown="span">US West</td>
      <td markdown="span">ReadWriteMany</td>
      <td markdown="span">NVME</td>
      <td markdown="span">22 TB</td>
    </tr>
  </tbody>
</table>

### Mounting

To test it, create a PersistentVolumeClaim with `seaweedfs-storage-hdd` storageClass:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: examplevol
spec:
  storageClassName: seaweedfs-storage-hdd
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: <volume size, f.e. 20Gi>
```

After you've created a PVC, you can see it's status (`kubectl get pvc pvc_name`). Once it has the Status `Bound`, you can attach it to your pod (claimName should match the name you gave your PVC):

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: vol-pod
spec:
  containers:
  - name: vol-container
    image: ubuntu
    args: ["sleep", "36500000"]
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  restartPolicy: Never
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: examplevol
```

It's also [possible to access your volume via WebDAV](https://github.com/chrislusf/seaweedfs/wiki/WebDAV) from your local machine.

Seaweedfs also [supports S3 and HadoopFS](https://github.com/chrislusf/seaweedfs/wiki)

