## Using storage

Different Kubernetes clusters will have different storage options available.
Let’s explore the most basic one: emptyDir. It will allocate local scratch volume, which will be gone once the pod is destroyed.

You can copy-and-paste the lines below.

###### strg1.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-storage
  labels:
    k8s-app: test-storage
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: test-storage
  template:
    metadata: 
      labels:
        k8s-app: test-storage
    spec:
      containers:
      - name: mypod
        image: alpine
        resources:
           limits:
             memory: 100Mi
             cpu: 100m
           requests:
             memory: 100Mi
             cpu: 100m
        command: ["sh", "-c", "apk add dumb-init && dumb-init -- sleep 100000"]
        volumeMounts:
        - name: mydata
          mountPath: /mnt/myscratch
      volumes:
      - name: mydata
        emptyDir: {}
```

Now let’s start the deployment:

`kubectl create -f strg1.yaml`

Now log into the created pod, create

`mkdir /mnt/myscratch/username`

then store some files in it.

Also put some files in some other (unrelated) directories.

Now kill the container: `kill 1` wait for a new one to be created, then log back in.

What happened to files?

You can now delete the deployment.

## Using outer persistent storage

In our cluster we have ceph storage connected, which allows using it for real data persistence.

To get storage, we need to create an abstraction called PersistentVolumeClaim. By doing that we "Claim" some storage space - "Persistent Volume". There will actually be PersistentVolume created, but it's a cluster-wide resource which you can not see.

Create the file:

###### pvc.yaml:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-vol
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

We're creating a 1GB volume and formatting it with XFS.

Look at it's status with `kubectl get pvc test-vol`. The `STATUS` field should be equals to `Bound` - this indicates successful allocation.

Now we can attach it to our pod. Create one:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod
spec:
  containers:
  - name: mypod
    image: centos:centos7
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 100Mi
        cpu: 100m
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: test-vol
```

In volumes section we're attaching the requested persistent volume to the pod (by its name!), and in volumeMounts we're mounting the attached volume to the container in specified folder.

## Exploring storageClasses

Attaching persistent storage is usually done based on storage class. Different clusters will have different storageClasses, and you have to read the [documentation](/userdocs/storage/intro) on which one to use. Some are restricted and you need to contact admins to ask for permission to use those.

Note that the one we used is the default - it will be used if you define none.

## Cleaning up

After you've deleted all the pods and deployments, delete the volume claim:

`kubectl delete pvc test-vol`

Please make sure you did not leave any running pods, deployments, volumes.

## End
