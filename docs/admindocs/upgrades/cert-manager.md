## Upgrading Cert-manager

Follow the [static manifest upgrade instructions](https://cert-manager.io/docs/installation/upgrading/#upgrading-using-static-manifests)