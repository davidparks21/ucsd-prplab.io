#### Upgrading seaweedfs

Only deployments image versions need to be upgraded.

The command is (set the latest version):

```bash
VERSION=3.55
kubectl -n seaweedfs set image deployments volume=chrislusf/seaweedfs:$VERSION --all
kubectl -n seaweedfs set image deployments master=chrislusf/seaweedfs:$VERSION --all
kubectl -n seaweedfs set image deployments filer=chrislusf/seaweedfs:$VERSION webdav=chrislusf/seaweedfs:$VERSION --all
```