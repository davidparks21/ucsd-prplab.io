#### Get started

[:rocket: Get access to Nautilus](/userdocs/start/get-access/)

[:octicons-terminal-16: Install tools](/userdocs/start/quickstart)

[:shield: Cluster policies](/userdocs/start/policies/)

[:question: FAQ](/userdocs/start/faq/)

[:material-contacts: Contact us](/userdocs/start/contact/)

[:book: Learn to use the cluster](/userdocs/tutorial/basic/)

#### Disclamers

The National Research Platform currently has no storage that is suitable for **HIPAA**, **PID**, **FISMA**, **FERPA**, or protected data of any kind. Users are not permitted to store such data on NRP machines.

#### Start using the cluster

[:fontawesome-solid-microchip: Run GPU pod](/userdocs/running/gpu-pods/)

[:material-card-multiple-outline: Run batch jobs](/userdocs/running/jobs/) (**:exclamation:Don't use "sleep" in those**); [:fontawesome-solid-rocket: make them run fast](/userdocs/running/io-jobs/)

[:material-docker: Explore the images we provide](/userdocs/running/sci-img/)

[:fontawesome-solid-hard-drive: Use persistent storage](/userdocs/storage/ceph-posix/) and [:material-cloud-upload: move your data in and out](/userdocs/storage/move-data/)

#### Other links

[:gem: Services you can use](/userdocs/start/resources/) (many require separate registration)

[:map: Cluster nodes map](https://elastic-igrok.nrp-nautilus.io/login?next=%2Fapp%2Fdashboards%3Fauth_provider_hint%3Danonymous1&auth_provider_hint=anonymous1#/view/76b9b030-81d5-11eb-ad7c-1f5ec373b923?_g=(filters%3A!())) and [:material-sitemap: Busy cluster map with all connections](https://traceroute.nrp-nautilus.io)
